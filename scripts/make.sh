#!/bin/bash

PROJECT_DIR="$(dirname $(dirname $(readlink -m $0)))"

MY_RPM=""


for ARG in $@
do
	case $ARG in
		"clean")
			echo "Cleaning up build directory"
			rm -rf ${PROJECT_DIR}/build
			;;
        "release")
        	echo "Building release version"
			MY_RPM="rpm"
            ;;
		"rpm")
			echo "Building rpm"
			MY_RPM="rpm"
			;;
		"srpm")
			echo "Building srpm"
			MY_RPM="srpm"
			;;
		*)
			echo "Unknown argument! Exiting..."
			exit 1
	esac
done

if [[ ! -d $PROJECT_DIR/build ]]
then
	echo "Creating build directory"
	mkdir $PROJECT_DIR/build
fi
MY_PROG_NAME="ndmspc-ansible"
if [ -n "$MY_PROJECT_VER" ];then
        MY_VER_MAJOR=$(echo "$MY_PROJECT_VER" | cut -d '.' -f1)
        MY_VER_MINOR=$(echo "$MY_PROJECT_VER" | cut -d '.' -f2)
        MY_VER_PATCH=$(echo "$MY_PROJECT_VER" | cut -d '.' -f3 | cut -d '-' -f1)
        MY_VER_RELEASE=$(echo "$MY_PROJECT_VER" | cut -sd '-' -f2)
		    [ -n "$MY_VER_RELEASE" ] || MY_VER_RELEASE="1"
        [[ $MY_VER_RELEASE =~ ^[[:digit:]] ]] || MY_VER_RELEASE="0.1.$MY_VER_RELEASE"
        sed -i 's/^project(.*/project('$MY_PROG_NAME' LANGUAGES NONE VERSION '$MY_VER_MAJOR'.'$MY_VER_MINOR'.'$MY_VER_PATCH' DESCRIPTION "NDMSPC ansible")/' CMakeLists.txt
        sed -i 's/^set(PROJECT_VERSION_RELEASE.*/set(PROJECT_VERSION_RELEASE '$MY_VER_RELEASE')/' CMakeLists.txt
        echo "Custom version : $MY_VER_MAJOR.$MY_VER_MINOR.$MY_VER_PATCH-$MY_VER_RELEASE"
fi

cd $PROJECT_DIR/build || { echo "Missing build directory!"; exit 1; }

cmake ${MY_CMAKE_OPTS} ../
[ -n "$MY_RPM" ] && make $MY_RPM

