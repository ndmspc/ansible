Summary: EOS storage hybrilit local config
Name: eos-config-hybrilit-local
Version: 1.0.3
Release: 1
Source0: auto.eos.hybrilit.local
Source1: fuse.hybrilit-jinr-ru-local.conf
BuildArch: noarch
Group: Applications/System
License: BSD
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: autofs
Requires: eos-config-base
Conflicts: eos-config-hybrilit

%description
Hybrilit configuration for EOS

%prep

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/auto.master.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/eos

for key in %{SOURCE0}; do
    install -D -m 444 "${key}" $RPM_BUILD_ROOT%{_sysconfdir}/
done

for key in %{SOURCE1}; do
    install -D -m 444 "${key}" $RPM_BUILD_ROOT%{_sysconfdir}/eos/
done

%files
%config %{_sysconfdir}/

%changelog

