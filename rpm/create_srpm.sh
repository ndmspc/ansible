#!/bin/bash

function help() {
  echo "Usage:"
  echo "  $0 hlit-admin"
  echo "  $0 hlit-test"
  exit 1
}

[ $# -lt 1 ] && help
RPMOPT="${2-"-ts"}"
[ -d $HOME/rpmbuild/SPECS ] || mkdir -p $HOME/rpmbuild/SPECS
[ -d $HOME/rpmbuild/SOURCES ] || mkdir -p $HOME/rpmbuild/SOURCES

echo "Doing $1 ..."
./maketar.sh $1
cd $1
rpmbuild $RPMOPT $(find . -name "*.tar.gz")
cd -
