Name:           eos-release
Version:        1.0.4
Release:        1%{?dist}
Summary:        EOS Repository Configuration

Group:          System Environment/Base
License:        BSD
URL:            http://eos.cern.ch
Source0:        eos-el.repo
Source1:        eos-fc.repo
#Source13:       RPM-GPG-KEY-rpmfusion-%{repo}-fedora-13-primary
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       system-release >= %{version}

# If apt is around, it needs to be a version with repomd support
Conflicts:      apt < 0.5.15lorg3

%description
EOS RPM repository contains open source and other distributable software for
Fedora.

%prep
echo "Nothing to prep"

%build
echo "Nothing to build"

%install
rm -rf $RPM_BUILD_ROOT

# Create dirs
#install -d -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg
install -d -m755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%if 0%{?rhel} == 7 || 0%{?el7} == 1 || 0%{?rhel} == 8 || 0%{?el8} == 1
%{__install} -p -m644 %{SOURCE0} \
    $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d/eos.repo
%endif
%if 0%{?fedora} >= 28
%{__install} -p -m644 %{SOURCE1} \
    $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d/eos.repo
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/yum.repos.d/*

%changelog
