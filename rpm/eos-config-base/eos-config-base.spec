Summary: EOS storage base config
Name: eos-config-base
Version: 1.0.1
Release: 1
Source0: eos.sh
Source1: auto.eos
Source2: eos.autofs
BuildArch: noarch
Group: Applications/System
License: BSD
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: autofs
Requires: eos-client, eos-fusex-core, autofs

%description
Hybrilit configuration for EOS

%prep

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/auto.master.d

for key in %{SOURCE0}; do
    install -D -m 644 "${key}" $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/
done

for key in %{SOURCE1}; do
    install -D -m 644 "${key}" $RPM_BUILD_ROOT%{_sysconfdir}/
done

for key in %{SOURCE2}; do
    install -D -m 644 "${key}" $RPM_BUILD_ROOT%{_sysconfdir}/auto.master.d/
done

%files
%config(noreplace) %{_sysconfdir}/

%changelog

