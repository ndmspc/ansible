Summary: CernVM File System Hybrilit Configuration and Public Keys
Name: cvmfs-config-hybrilit
Version: 1.0.1
Release: 1
Source0: hybrilit.jinr.ru.conf
Source1: hybrilit.jinr.ru.pub
BuildArch: noarch
Group: Applications/System
License: BSD
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Provides: cvmfs-config = %{version}-%{release}
Obsoletes: cvmfs-keys < 1.6
Provides: cvmfs-keys = 1.6
Obsoletes: cvmfs-init-scripts < 1.0.21
Provides: cvmfs-init-scripts = 1.0.21

Conflicts: cvmfs < 2.1.20
Conflicts: cvmfs-server < 2.1.20

Requires: autofs cvmfs cvmfs-config-default

%description
Hybrilit configuration parameters and public keys for CernVM-FS, providing access
to repositories under the cern.ch, egi.eu, and opensciencegrid.org domains

%prep

%install
rm -rf $RPM_BUILD_ROOT
for cvmfsdir in keys/jinr.ru config.d; do
    mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/cvmfs/$cvmfsdir
done
for key in %{SOURCE1}; do
    install -D -m 444 "${key}" $RPM_BUILD_ROOT%{_sysconfdir}/cvmfs/keys/jinr.ru
done
for conf in %{SOURCE0}; do
    install -D -m 444 "${conf}" $RPM_BUILD_ROOT%{_sysconfdir}/cvmfs/config.d
done

%files
%dir %{_sysconfdir}/cvmfs/keys/jinr.ru
%{_sysconfdir}/cvmfs/keys/jinr.ru/*
%config %{_sysconfdir}/cvmfs/config.d/*

%changelog
* Thu Jan 05 2017 Martin Vala <mvala@saske.sk> - 1.0-2
- Initial packaging

