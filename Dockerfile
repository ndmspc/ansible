FROM registry.fedoraproject.org/fedora-toolbox:34
RUN mkdir -p /playbooks
COPY ansible.cfg /playbooks/
COPY hosts /playbooks/
COPY roles/ /playbooks/roles
COPY *.yml /playbooks/
WORKDIR /playbooks
RUN dnf update -y && \
    dnf install ansible -y && \
    ansible-galaxy role install -r requirements.yml && \
    ansible-galaxy collection install -r requirements.yml && \
    ansible-playbook local.yml -e ansible_python_interpreter=/usr/bin/python3 -t hep,root,web,vscode
RUN dnf clean all

CMD /bin/bash
