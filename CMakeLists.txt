cmake_minimum_required(VERSION 3.11 FATAL_ERROR)
project(ndmspc-ansible LANGUAGES NONE VERSION 0.1.0 DESCRIPTION "NDMSPC ansible")
set(PROJECT_VERSION_RELEASE 1)
include(cmake/cmake_base.cmake)
